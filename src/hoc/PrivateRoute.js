import { Route, Redirect } from "react-router-dom";
import { AppRoutes } from "../consts/AppRoutes";
import {AppContext} from "../AppProvider";
import {useContext} from "react";

export const PrivateRoute = props => { //Redirects to login page if not logged in

    const [session] = useContext(AppContext)

    if (!session) {
        return <Redirect to={ AppRoutes.Login }  />
    }

    return <Route {...props} />
}

export default PrivateRoute
