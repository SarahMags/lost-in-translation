import {Redirect, Route} from "react-router-dom";
import {AppRoutes} from "../consts/AppRoutes";
import {useContext} from "react";
import {AppContext} from "../AppProvider";

export const PublicRoute = props => {
    const [session] = useContext(AppContext)


    /**
     * Since login is the only public route, we redirect to the translate
     * page when logged in, to avoid the user getting back to the login page
     * **/
    if (session) {
        return <Redirect to={ AppRoutes.Translate }  />
    }
    return <Route {...props} />
}

export default PublicRoute
