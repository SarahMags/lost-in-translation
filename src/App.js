import './App.css';
import { BrowserRouter, Switch } from 'react-router-dom'
import PrivateRoute from "./hoc/PrivateRoute";
import PublicRoute from "./hoc/PublicRoute";
import { AppRoutes } from "./consts/AppRoutes";
import Login from "./components/Login";
import Translate from "./components/Translate";
import Profile from "./components/Profile";
import Navbar from "./components/Navbar";
import { AppProvider } from './AppProvider'


function App() {
  return (
      <AppProvider>
          <BrowserRouter>
              <div className="app">
                  <Navbar />
                  <Switch>
                    <PublicRoute path={ AppRoutes.Login } exact component={ Login } />
                    <PrivateRoute path={ AppRoutes.Translate } component={ Translate } />
                    <PrivateRoute path={ AppRoutes.Profile } component={ Profile } />
                  </Switch>
              </div>
          </BrowserRouter>
      </AppProvider>
  );
}

export default App;
