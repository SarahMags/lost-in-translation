import {createContext, useState} from 'react'
import {LocalStorage} from "./util/storage";
import {loginKey} from "./components/Login";

export const AppContext = createContext()

export function AppProvider(props) {
    const [session, setSession] = useState(LocalStorage.get(loginKey))


    return <AppContext.Provider value={ [session, setSession]}>
        {props.children}
    </AppContext.Provider>
}