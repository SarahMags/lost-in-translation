import styled from "styled-components";

//CSS for all components

//Login.js
export const StyledLoginDiv = styled.div`
   width: 100%;
   background: linear-gradient(#ffc75f 83%, #ffffff 17%);

   
   fieldset {
       background-color: white;
       width: 50%;
       padding: 25px 35px;
       border-radius: 17px;
       border-style: none;
       box-shadow: 0 0 15px 0px grey;
       margin-top: 14px;
       background: linear-gradient(white 82%, #845EC2 18%);
      
   }
   
   .loginContent{
    display: flex;
    margin-left: 15%;
    }
    
    .image-wrapper {
        position: relative;
        height: 200px;
        width: 200px;
        margin-left: 80px;
        
    }
    
    .cloudImg {
        width: 100%;
        height: 100%;
    }
    
    .robotImg{
        position: absolute;
        height: 80%;
        left: 0;
        right: 0;
        margin: auto;
        top: 2em;
        bottom: 0;
    }
    
    .loginText{
        margin-left: 9%;
        margin-top: 6%;
    }
    
    .userForm{
        margin-left: 19%;
    }
    
    input, input:focus {

        border: hidden;
        outline: none;
        font-family: Sanchez, serif;
        font-size: 1.5em;
        background-color: transparent;
    }
   
    
    .roundInputField {
        background-color: #EEEEEE;
        border-radius: 2em;
        border: 3px solid #BEBEBE;
        display:flex;
        margin-bottom: 2em;
    }
    
    .material-icons {
        font-family: 'Material Icons';
        background-color: #845EC2;
        font-size: 2.7em;
        color: white;
        border-radius: 50%;
        border: transparent;
        padding: 2px 2px;
        margin: .1em .1em .1em 7em;
        
    }
    
    .material-icons-outlined {
        font-family: 'Material Icons';
        font-size: 2.7em;
        border-radius: 50%;
        border: transparent;
        padding: 2px 2px;
        margin: .1em .3em;
        color: grey;
    }
    
    
    
`
//Profile.js
export const Translations = styled.div`
     background-color: white;
     min-height: 50vh;
     height: AUTO;
     width: 65%;
     padding: 25px 35px;
     border-radius: 17px;
     border-style: none;
     box-shadow: 0 0 15px 0px grey;
     margin-top: 14px;
     margin: auto;
     font-family: Sanchez, serif;
     font-size: 20px;
     
     p{
        margin-left: 4%;
     }
     
`
export const BackgroundColor = styled.div`
       background: linear-gradient(#ffc75f 83%, #ffffff 17%);
       
       h1{
            padding-bottom: 1em;
            margin-bottom: .5em;
       }
       
       button{
            color: white;
            background-color: #845EC2;
            font-size: 1em;
            border-radius: 10px;
            font-family: Sanchez, serif;
            border: 0;
            padding: .5em .5em;
            margin-left: 1em;
            margin-top: 1em;
       }
    `

//Translate.js
export const SignBox = styled.div`
     background-color: white;
     min-height: 50vh;
     height: AUTO;
     width: 73%;
     padding: 25px 35px;
     border-radius: 17px;
     border-style: none;
     box-shadow: 0 0 15px 0px grey;
     margin-top: 14px;
     background: linear-gradient(white 90%, #845EC2 10%);
     margin: auto;


     img{
        max-width: 10%;

     }

`

export const StyledSearchDiv = styled.div`
    background: linear-gradient(#ffc75f 83%, #ffffff 17%);
    height: 23vh;
    padding-top: 3%;
`

export const StyledSearchBar = styled.div`
    background-color: #EEEEEE;
    border-radius: 2em;
    border: 3px solid white;
    display:flex;
    width: 80%;
    display: flex; 
    margin: auto;   
    
    
    input, input:focus {
        border: hidden;
        outline: none;
        font-family: Sanchez, serif;
        font-size: 1.5em;
        background-color: transparent;
        width: 78%;
    }
    
    .material-icons {
        font-family: 'Material Icons';
        background-color: #845EC2;
        font-size: 2.7em;
        color: white;
        border-radius: 50%;
        border: transparent;
        padding: 2px 2px;
        margin: .1em .1em .1em 2em;
        
    }
    
    .material-icons-outlined {
        font-family: 'Material Icons';
        font-size: 2.7em;
        border-radius: 50%;
        border: transparent;
        padding: 2px 2px;
        margin: .1em .3em;
        color: grey;
    }

`

//Navbar.js
export const StyledNavbar = styled.div`
   width: calc(100% - 2em);
   background-color: #FFC75F;
   border-bottom: 2px solid #E7B355;
   padding: 1em;
   
   h1 { 
    margin: 0 5em ;
    display: inline;
    color: white;
    font-size: 20px;
    font-weight: 100;
   }
   
   #navBtns {
   float: right;
   margin-top: -15px;

   }
   
   .profileButton{
        display: flex;
     
   
   }
   
    .material-icons {
        font-family: 'Material Icons';
        background-color: white;
        font-size: 2.7em;
        color: #E7B355;
        border-radius: 50%;
        border: transparent;
        padding: 2px 2px;
        margin: .1em .1em .1em 0.3em;
        
    }
    
    #username{
        color: white;
        font-weight: 100;
        border: 2px solid #E7B355;
        background-color: #E7B355;
        border-radius: 8px;   
        
    }
 
`
