import {useHistory} from "react-router-dom";
import {AppContext} from "../AppProvider";
import {useContext} from 'react';
import {BackgroundColor,Translations} from "./Styles"; //Css


function Profile() {

    const [, setSession] = useContext(AppContext) //setting session to AppContext
    const history = useHistory(); //History used to redirect on button click

    const onBackClick = () => { //redirects back to translate page
        history.push('/translate')
    }

    const onLogOutClick = () => { //clears session and local storage, redirects to login page
        setSession(null)
        localStorage.clear()
        history.push('/')
    }

    return (
        <div>
            <BackgroundColor>
                <button type="button" onClick={onBackClick}>Back</button>
                <button type="button" onClick={onLogOutClick}>Log out</button>
                <h1>Latest translations</h1>
            </BackgroundColor>
            <Translations>
                <p>1. {localStorage.getItem('0')}</p>
                <p>2. {localStorage.getItem('1')}</p>
                <p>3. {localStorage.getItem('2')}</p>
                <p>4. {localStorage.getItem('3')}</p>
                <p>5. {localStorage.getItem('4')}</p>
                <p>6. {localStorage.getItem('5')}</p>
                <p>7. {localStorage.getItem('6')}</p>
                <p>8. {localStorage.getItem('7')}</p>
                <p>9. {localStorage.getItem('8')}</p>
                <p>10. {localStorage.getItem('9')}</p>
            </Translations>
        </div>
    )
}

export default Profile;