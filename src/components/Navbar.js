import { useHistory } from 'react-router-dom';
import {useEffect, useContext} from "react";
import {AppContext} from "../AppProvider";
import {StyledNavbar} from "./Styles";


function Navbar() {
    const [session] = useContext(AppContext); //Using the app providers App context to render profile button when logged in
    const history = useHistory(); // History variable used as dependency in useEffect and to redirect on button click

    console.log('session', session)

    useEffect(() => { //Re-renders navbar when logged in
        console.log("running")
    }, [ history])

    const onProfileClick = () => { //Redirects to profile component
        history.push('/profile')
    }

    return (
        <StyledNavbar>
            <h1>Lost in Translation</h1>
            <div id="navBtns">
                { session && //If session from AppProvider is true, render profile button and username
                    <div className="profileButton">
                        <p id="username">{session}</p>
                        <button className="material-icons" type="button" onClick={ onProfileClick }>account_circle</button>
                    </div>
                }

            </div>
        </StyledNavbar>
    )
}

export default Navbar;