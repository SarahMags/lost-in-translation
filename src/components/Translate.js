import {useState} from 'react';
import { LetterToSign } from "../consts/LetterToSign";
import {LocalStorage} from "../util/storage";
import {StyledSearchBar,StyledSearchDiv, SignBox} from "./Styles";

let searchCount = 0; //Counter to keep track of translation keys for local storage


if ( localStorage.getItem('searchCounter') !== null) { //if a counter is stored, continue from where it left off
    searchCount = localStorage.getItem('searchCounter')
}

function Translate() {

    const [searchText, setSearchText] = useState('')
    const [searchList, setSearchList] = useState([])
    let searchID = '';

    const onSearchChange = (e) => {
        setSearchText(e.target.value.trim().toLowerCase()) //set them all to lower case
    }

    const onTranslateClick = () => { //puts search string into a list of characters, and stores the strings in localstorage
        setSearchList([...searchText]) //creates a space between each character
        searchID = searchCount; //Counter is set to last stored counter variable from local storage
        searchCount++;
        LocalStorage.set('searchCounter', searchCount) //counter stays between 0-9 to keep the keys, and only update values
        if (searchCount > 9){
            searchCount = 0;
        }
        LocalStorage.set(searchID, searchText) //SearchID is number from 0-9, searchText is input from field
    }

    return (
        <div>
            <StyledSearchDiv>
                    <StyledSearchBar>
                        <span className="material-icons-outlined">keyboard</span>
                        <input onChange = { onSearchChange }
                               className = "searchName"
                               type="text"
                               placeholder="Write something you want to translate" />
                        <button className="material-icons" type="button" onClick={ onTranslateClick }>arrow_forward</button>
                    </StyledSearchBar>
            </StyledSearchDiv>
                <SignBox>
                    {searchList.map((char, key) => //Prints images using LetterToSign (characters are keys, paths are values)
                        <img src={ LetterToSign[char] } key={key} alt="Sign for character" />
                    )}
                </SignBox>
        </div>

    )

}
export default Translate