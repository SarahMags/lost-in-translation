import { useState, useContext } from "react";
import { LocalStorage } from "../util/storage";
import { useHistory } from 'react-router-dom';
import {AppContext} from "../AppProvider";
import {StyledLoginDiv} from "./Styles";

export const loginKey = 'Username';

function Login() {

    const history = useHistory();
    const [, setSession] = useContext(AppContext)

    const [state, setState] = useState({ //Setting initial state for key empty
        username: ''
    })


    const onInputChange = e => {  //Setting the username state when input changes
        setState({
            ...state,
            [e.target.id]: e.target.value
        })
    }

    //Stores username in local- and session storage, redirects to translate page if input is not empty
    const onLoginClick = async () => {
        if( state.username !== '' ){
            LocalStorage.set( loginKey, state.username)
            setSession(state.username)
            history.push('/translate')
        }
    }

    return (
        <StyledLoginDiv>
            <div className="loginContent">
                <div className="image-wrapper">
                    <img className="cloudImg" src={ "resources/Splash.svg" } alt="Splash" />
                    <img className="robotImg" src={"resources/Logo.png"} alt="Robot logo" />
                </div>
                <div className= "loginText" >
                    <h1>Lost in Translation</h1>
                    <h2>Get started</h2>
                </div>
            </div>
            <div className="userForm">
                <form>
                    <fieldset>
                        <div className="roundInputField">
                            <span className="material-icons-outlined">keyboard</span>
                            <input onChange={ onInputChange } id="username" type="text" placeholder="What's your name?" />
                            <button className="material-icons" type="button" onClick={ onLoginClick }>arrow_forward</button>
                        </div>
                    </fieldset>
                </form>
            </div>
        </StyledLoginDiv>
    )
}
export default Login;