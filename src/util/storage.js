export const LocalStorage = { //Chose not to encode keys and input for learning purposes
    get(key) { //Get stored item by key
        const stored = localStorage.getItem(key)
        if (!stored) {
            return null
        }
        try {
            //const decoded = atob(stored)
            return JSON.parse(stored)
        } catch (e) {
            return null
        }
    },
    set(key, value) { //Set item in local storage, input key and value as json
        try {
            const json = JSON.stringify(value)
            //const encoded = btoa(json)
            localStorage.setItem(key, json)
        } catch (e) {
            console.log(e.message)
        }
    }
}