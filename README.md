# Lost in Translation 

This project is a web application created with React. 
The applications main feature is to act as a translator. The user can translate 
English words and sentences to American sign language. 

The application consists of three different pages:
- Startup/Login page
- Translation page
- Profile page

### Startup/Login page

The first thing a user can see is the "Login page". The user has to type in their name before they can
use the translator. When the user has typed in their name it is stored in local storage. 

### Translation page

The user can type in a chosen word or a sentence into the input box. By clicking the "translation" button
the word or sentence will be displayed into sign language in the box below.  
The translations are stored by local storage up to 10 translations. This can be viewed in the "Profile page". 

### Profile page

The profile page displays the last 10 translations for the current user. There is also a "back" button to return to the translator 
page and a "log out" button which clears all the storage and returns to the start page.

Project repo can be found at:
[GitLab](git@gitlab.com:SarahMags/lost-in-translation.git) 




Created by,

Sarah Fagerhol Thorstensen and 
Maren Ytterdal Krogsrud